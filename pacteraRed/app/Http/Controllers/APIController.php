<?php

namespace App\Http\Controllers;

use App\Jobs\SendPacketJob;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Controllers\FunController;

class APIController extends BaseController
{

    public function addRedisList(Request $request)
    {
        $access_token = $request->post('access_token');
        Redis::select(0);
        [$status, $lname] = FunController::getStatusLname();

        $stitch = Redis::get('redSwitch');
        if (boolval($stitch) & $status > 0) {
            $data = explode(':', Redis::get($access_token));

            if ($data[1] < $status) {
                Redis::select(4);

                if (Redis::command('LLEN', [$lname]) > 0) {
                    $moneyData = explode(':', Redis::command('LPOP', [$lname]));
                    Redis::select(0);
                    Redis::set($access_token, $data[0] . ':' . $status);
                    dispatch(new SendPacketJob($data[0] . ':' . $moneyData[0] . ':0'));
                    $response = ['coda' => 1, 'msg' => $moneyData[1]];
                } else {
                    $response = ['coda' => -1, 'msg' => '红包已发完！'];
                }
            } else {
                $response = ['coda' => 0, 'msg' => '已发放红包！'];
            }
        } else {
            $response = ['coda' => -1, 'msg' => '红包已发完！'];
        }

        return response()->json($response);
    }
    //
//    public function sendRedPackets(Request $request)
//    {
//        $access_token = $request->cookie('access_token');
//        Redis::select(0);
//        $time = time();
//
//        if ($time > env('FDAY_START') && $time <= env('FDAY_END')) {
//            $status = 2;
//            $lname = 'firstDay';
//        } elseif($time > env('FDAY_END') && $time <= env('SDAY_END')) {
//            $status = 3;
//            $lname = 'SecondDay';
//        } elseif(!env('FORMAL')) {
//            $status = 1;
//            $lname = 'firstDay';
//        } else {
//            $status = -1;
//        }
//
//        if ($access_token && Redis::command('EXISTS',[$access_token]) == 1 && (!env('FORMAL') || $status > 0)) {
//            $data = explode(':', Redis::get($access_token));
//            if ($data[1] < $status) {
////              发放红包
//                $config = [
//                    'app_id'    => 'wx7f4a9ce4e47b8ebe',
//                    'mch_id'    => '1484813662',
//                    'key'       => '79bd840a4e1c4e334a59a51bb6bdf27c',
//                    'cert_path' => base_path('certificate/apiclient_cert.pem'),
//                    'key_path'  => base_path('certificate/apiclient_key.pem')
//                ];
//                $payment = Factory::payment($config);
//
//                $redpack = $payment->redpack;
//                $moneyData = explode(':',$this->getMoney($lname));
//                $redpackData = [
//                    'mch_billno'   => str_random(5).time(),
//                    'send_name'    => '文思海辉',
////                    're_openid'    => $data[0],
//                    're_openid'    => $data[0],
//                    'total_num'    => 1,  //固定为1，可不传
//                    'total_amount' => $moneyData[0]*100,  //单位为分，不小于100
//                    'wishing'      => '心想事成，旺事如意',
//                    'client_ip'    => '192.168.0.1',  //可不传，不传则由 SDK 取当前客户端 IP
//                    'act_name'     => '文思海辉',
//                    'remark'       => '你参与 文思海辉数说头条摇一摇活动，成功获得文思海辉赠送的红包，点击消息打开，一起抢红包、拼手气吧',
//                ];
//
//                $result = $redpack->sendNormal($redpackData);
//
//                if ($result['result_code'] == 'FAIL') {
//
//                    Redis::select(4);
//                    Redis::command('LPUSH',[$lname,$moneyData[0].':'.$moneyData[1]]);
////                    失败
//                    $response = ['coda'=>-1];
//                } else {
////                    成功
//                    Redis::select(0);
//                    Redis::set($access_token,$data[0].':'.$status);
//                    $response = ['coda'=>1,'msg'=>$moneyData[1]];
//                }
//            } else {
////                领过奖
//                $response = ['coda'=>0,'msg'=>'已发放红包！'];
//            }
//
//            Redis::select(2);
//            Redis::command('INCR',[$access_token]);
//
//        } else {
//            //不存在$access_token
//            $response = ['coda'=>-1,'msg'=>'不存在access_token'];
//        }
//
//        return response()->json($response);
//    }

    public function share(Request $request)
    {
        $access_token = $request->post('access_token');
        Redis::select(0);
        if ($access_token && Redis::command('EXISTS', [$access_token]) == 1) {
            $data = Redis::get('shareNum');
            $data = empty($data) ? 1 : $data;

            Redis::set('shareNum', ++$data);
        }
    }


    public function affirm(Request $request)
    {
        $coda = $request->get('coda');
        $user = $request->get('user');

        if ($user == 'admin' && is_numeric($coda)) {
            Redis::set('projectswitch', $coda);
            return response()->json(['msg' => '完成']);
        }

        return response()->json(['msg' => '失败']);
    }

    public function add(Request $request)
    {

        if ($request->get('user') == 'admin') {
            $lname = $request->get('lname');
            $num = $request->get('num');
            $money = $request->get('money');
            $arr = FunController::createRedPackage($money);
            $count = count($arr);
            Redis::select(4);

            foreach ($arr as $key => $value) {
                Redis::command('LPUSH',[$lname,$value.':'.($num + $count - $key)]);
            }

            return response()->json(['count' => $count]);
        }
    }

    public function test()
    {
        dd(FunController::getStatusLname());
    }
}

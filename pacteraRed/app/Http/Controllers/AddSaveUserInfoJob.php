<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Ali_Log;
use App\Jobs\SaveUserInfo;

class AddSaveUserInfoJob extends Controller
{

    public static function addJob($userArr)
    {
        $userJob = new SaveUserInfo($userArr);
        dispatch($userJob);
    }
}

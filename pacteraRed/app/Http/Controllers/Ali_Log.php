<?php

namespace App\Http\Controllers;

use Aliyun_Log_Client;
use Aliyun_Log_Models_LogItem;
use Aliyun_Log_Models_PutLogsRequest;
use Aliyun_Log_Exception;

class Ali_Log extends Controller
{
    public static $client = '';

    public static function construct()
    {
        $endpoint = env('ALI_LOG_endpoint'); // 选择与上面步骤创建 project 所属区域匹配的 Endpoint
        $accessKeyId = env('ALI_ACCESS_KEY');        // 使用你的阿里云访问秘钥 AccessKeyId
        $accessKey = env('ALI_ACCESS_SECRET');            // 使用你的阿里云访问秘钥 AccessKeySecret
        // 上面步骤创建的日志库名称

        self::$client = new Aliyun_Log_Client($endpoint, $accessKeyId, $accessKey);
    }

    private static function PutLog($msg, $type = 'info')
    {
        $project = env('ALI_LOG_project');                  // 项目名称
        $logstore = $type ?? env('ALI_LOG_logstore');

        $topic = "";
        $source = "";
        $contents = array('msg' => strval($msg));
        $logItem = new Aliyun_Log_Models_LogItem();
        $logItem->setTime(time());
        $logItem->setContents($contents);
        $logitems = array($logItem);

        $request = new Aliyun_Log_Models_PutLogsRequest($project, $logstore, $topic, $source, $logitems);
        self::construct();
        $client = self::$client;
        echo '<pre>';
        try {
            $response = $client->putLogs($request);
//            var_dump($response);
        } catch (Aliyun_Log_Exception $ex) {
//            var_dump($ex);
        } catch (\Exception $ex) {
//            var_dump($ex);
        }

    }

    public static function info($msg)
    {
        if (!app()->environment('local')) {

            self::PutLog($msg, 'info');
        }
    }

    public static function error($msg)
    {
        if (!app()->environment('local')) {
            self::PutLog($msg, 'error');
        }
    }
}

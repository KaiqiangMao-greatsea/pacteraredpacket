<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Ali_Log;
use App\Http\Controllers\AddSaveUserInfoJob;
use App\Models\WechatUsersInfo;
use App\Jobs\SendPacketJob;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;


class ErrorSendController extends Controller
{

    protected $log = '';
    protected $errorFileName = './error.log';
    protected $sendLog = './send.log';

    public function __construct()
    {
        $this->log = fopen(storage_path('logs/lumen02151346.log'), 'r');
    }

    public function getError()
    {
        while (!feof($this->log)) {
            $str = fgets($this->log);
            var_dump($str);
            if (strstr($str, 'lumen.ERROR')) {
                $this->seaveError($str);
            }
        }
    }

    public function seaveError($str)
    {
        $handle = fopen($this->errorFileName, 'a+');
        fwrite($handle, $str);
        fclose($handle);
    }

    public function addJob()
    {
        $handle = fopen($this->errorFileName, 'r');
        $sendHandle = fopen($this->sendLog, 'w');
        $i = 0;
        while (!feof($handle)) {
            $str = fgets($handle);
            $arr = explode(':', $str);
            if (count($arr) > 6) {
                $jobStr = $arr[4] . ':' . $arr[6] . ':0';
                fwrite($sendHandle, $arr[4] . ',');
                var_dump($jobStr . '<br>');
                var_dump(++$i);
                dispatch(new SendPacketJob($jobStr));
            }
        }
    }

}

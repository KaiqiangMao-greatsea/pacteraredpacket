<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Ali_Log;
use App\Http\Controllers\AddSaveUserInfoJob;
use App\Models\WechatUsersInfo;
use App\Jobs\SendPacketJob;
use Illuminate\Support\Facades\Redis;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function test(WechatUsersInfo $wechatUsersInfo)
    {
        var_dump(opcache_reset());
//        for ($i = 0; $i < 3; $i++) {
//            print_r('测试发一个任务' . PHP_EOL);
//            dispatch(new SendPacketJob('test987' . ':' . '12' . ':0'));
//        }
//        Ali_Log::info('test123');
//        $str = str_random(3);
//        $userArr = [
//            'openid' => 'o2pquxCieRVwRliN0LJLFigCiAhY',
//            'nickname' => '关掉tV',
//            'sex' => 1,
//            'language' => 'zh_CN',
//            'city' => '四平',
//            'province' => '吉林',
//            'country' => '中国',
//            'pri' => '中国',
//            'headimgurl' => 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erX476ictOkz7feWg8FRiblOasVibzIGuiaibVOzpx8OxvOnCzMN4RbNRicX4Ns9WM2R3ibQM3XgytciabBzQ/132',
//            'unionid' => 'oxcSOwIXHKMvUpOtp4XxL7YIFVpY' . $str
//        ];

//        $fill = ['openid', 'unionid', 'nickname', 'sex', 'language', 'city', 'province', 'country', 'headimgurl'];
//        $tmp = [];
//        foreach ($fill as $v) {
//            $tmp[$v] = $userArr[$v];
//        }
//        if (count($tmp) == 9) {
//            $wechatUsersInfo->addWechatUser($tmp);
//        }

//        AddSaveUserInfoJob::addJob($userArr);
//        $url = env('RESOURCE_LINK');
//
//        $arr = [
//            'en-pic1_03.png',
//            'en-pic1_11.png',
//            'en-pic1_15.png',
//            'en-pic1_18.png',
//            'en-pic1_23.png',
//            'en-pic1_26.png',
//            'en-pic1_30.png',
//            'en-pic1_35.png',
//            'zj_02.png',
//            'repeat1_02.png'
//        ];
//        $int = rand(100,200);
//        for ($i = 0; $i <= 7; $i++) {
//            $int = rand(1000,20000);
//            echo <<<EOF
//<img src="{$url}/images/{$arr[$i]}?id={$int}" class="yly">
//EOF;
//
//        }
    }

    public function redLimit()
    {
        Redis::select(4);

        $lent = Redis::command("LLEN", ["fourthDay"]);

        if ($lent <= 1700) {
            $res = file_get_contents("http://39.106.219.124/zvvkquwerEdvELf3JTy3wa/switch/0");
            return response()->json(["msg" => "红包剩余个数小于1700，已关闭活动红包。" . $res, "remain" => $lent, "code" => 500]);
        }

        return response()->json(["msg" => "现在剩余红包数：" . $lent, "remain" => $lent, "code" => 200]);
    }
}

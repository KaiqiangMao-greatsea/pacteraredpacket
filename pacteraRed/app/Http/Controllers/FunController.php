<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;

class FunController extends BaseController
{
    //
    public static function getStatusLname()
    {
        $time = time();
        $lname = '';
        $data = Redis::get('projectswitch');

        if (empty($data) || $data != 1) {
            $arr = ['FDAY_START' => 1518623999,
                'FDAY_END' => 1518710399,
                'SDAY_START' => 1518710399,
                'SDAY_END' => 1518796799,
                'FLNAME' => 'firstDay',
                'FSTATUS' => 2,
                'SLNAME' => 'SecondDay',
                'SSTATUS' => 3];
        } else {
            $arr = [
                'FDAY_START' => 1518883199,
                'FDAY_END' => 1518969599,
                'SDAY_START' => 1518969599,
                'SDAY_END' => 1519055999,
                'FLNAME' => 'thirdDay',
                'FSTATUS' => 4,
                'SLNAME' => 'fourthDay',
                'SSTATUS' => 5,
            ];
        }

        if ($time > $arr['FDAY_START'] && $time <= $arr['FDAY_END']) {
            $status = $arr['FSTATUS'];
            $lname = $arr['FLNAME'];
        } elseif ($time > $arr['SDAY_START'] && $time <= $arr['SDAY_END']) {
            $status = $arr['SSTATUS'];
            $lname = $arr['SLNAME'];
        } elseif (!env('FORMAL')) {
            $status = 1;
            $lname = 'firstDay';
        } else {
            $status = -1;
        }

        return [$status, $lname];
    }

    public static function createRedPackage($data)
    {
        $count = $data * 100;
        $arr = [];
        $num = 1;

        while (1 > 0) {
            if ($count <= 150) {
                if ($count < 100) {
                    $count = $data * 100;
                    $arr = [];
                } else {
                    $arr[] = $count / 100;
                    break;
                }
            }
            $x = rand(100, 150);
            $arr[] = $x / 100;
            $count -= $x;
            $num++;
        }

        return $arr;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;

class PacketNumberController extends BaseController
{
    //
    public function index()
    {
        Redis::select(4);

        if (Redis::get('status') == 1) {
            return response()->json(['coda'=>1]);
        }
        set_time_limit(300);
        $fday_arr = array(
            array(
                'rate' => 0.75,
                'rand' => [100,300]
            ),
            array(
                'rate' => 0.2,
                'rand' => [300,500]
            ),
            array(
                'rate' => 0.05,
                'rand' => [500,700]
            ),
        );
        $sday_arr = array(
            array(
                'rate' => 0.8,
                'rand' => [100,300]
            ),
            array(
                'rate' => 0.17,
                'rand' => [300,500]
            ),
            array(
                'rate' => 0.03,
                'rand' => [500,700]
            ),
        );
        $fpacket = $this->getArr(7778, $fday_arr, 20 ,10);
        $spacket = $this->getArr(4844.6, $sday_arr, 10, 10);

        $this->insertRedis($fpacket, 'firstDay');
        $this->insertRedis($spacket, 'SecondDay');
        $this->insertRedis($spacket, 'thirdDay');
        $this->insertRedis($spacket, 'fourthDay');
        Redis::set('status',1);

        return response()->json(['fday'=>count($fpacket), 'sday'=>count($spacket)]);
    }

    public function getNum($amount, $rate, $rand)
    {
        $data = $rate * $amount;
        $arr = [];
        $num = 1;

        while (1 > 0) {
            if ($data <= $rand[1] / 100) {
//                if ($num >= $fcount) {
                $arr[] = $data;
                    break;
//                } else {
//                    $num = 1;
//                    $arr = [];
//                }
            }
            $x = rand($rand[0], $rand[1]) / 100;
            if ($x == 4.44) {
                continue;
            }
            $arr[] = $x;
            $data -= $x;
            $num++;
        }

        return $arr;
    }


    public function getArr($amount, $arr, $six, $eight)
    {
        $result = [];
        foreach ($arr as $value) {
            $res = $this->getNum($amount, $value['rate'], $value['rand']);
            $result = array_merge($res, $result);
        }

        $result = $this->insertArr($result, $six, 6.66);
        $result = $this->insertArr($result, $eight, 8.88);
        shuffle($result);

        return $result;
    }


    public function insertArr($arr, $num, $value)
    {
        for($i = 1; $i <= $num; $i++) {
            $key = $i * 80;
            array_splice($arr, $key, 0, $value);
        }

        return $arr;
    }

    public function insertRedis($arr, $lname)
    {
        $count = count($arr);
        foreach ($arr as $key => $value) {
            Redis::command('LPUSH',[$lname,$value.':'.($count - $key)]);
        }
    }
}

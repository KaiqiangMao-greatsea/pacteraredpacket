<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Routing\Controller as BaseController;
use EasyWeChat\Factory;
use \Symfony\Component\HttpFoundation\Cookie as SCookie;
use App\Http\Controllers\AddSaveUserInfoJob;
use Symfony\Component\Cache\Simple\RedisCache;


class PacteraController extends BaseController
{
    protected function app($config, $callback = false)
    {

        if ($callback) {
            $config['oauth']['callback'] = $callback;
        }

        return Factory::officialAccount($config);
    }

    //
    public function login(Request $request)
    {
        $app = $this->app(\GuzzleHttp\json_decode(env('WSHH_WECHAT'), true), '/vedio');
        //        集群access_token缓存
        $predis = app('redis')->connection()->client();
        $cache = new RedisCache($predis);
        $app['cache'] = $cache;
        //        集群access_token缓存

        $response = $app->oauth->scopes(['snsapi_base'])->setRequest($request)->redirect();

        return $response;
    }

//    public function callback(Request $request)
//    {
//        $app = $this->app(\GuzzleHttp\json_decode(env('WSHH_WECHAT'), true),'/wechat/callbackSILENCE');
//        if (!Cookie::get('access_token')) {
//            //        获取用户信息
//            $userData = $app->oauth->user();
//            $user_json = \GuzzleHttp\json_encode($userData);
//            AddSaveUserInfoJob::addJob($userData['original']);
//        }
//        $response = $app->oauth->scopes(['snsapi_base'])->setRequest($request)->redirect();
//
//        return $response;
//    }

//    public function silenceCallBack(Request $request)
//    {
//        try {
//            $app = $this->app(\GuzzleHttp\json_decode(env('WSHH_WECHAT'), true));
//            $userData = $app->oauth->user();
//            Redis::select(1);
//
//            if (Redis::command('EXISTS', [$userData['id']]) == 0) {
//                $api_token = str_random(60);
//                Redis::set($userData['id'], $api_token);
//                Redis::select(0);
//                Redis::set($api_token, $userData['id'] . ':0');
//                Redis::select(2);
//                Redis::set($api_token, 0);
//            } else {
//                $api_token = Redis::get($userData['id']);
//            }
//
//            return redirect('/vedio')->withCookie(new SCookie('access_token', $api_token, 'Fri, 31 Dec 9999 23:59:59 GMT'));
//        } catch (\Exception $e) {
//            return redirect('/vedio2');
//        }
//    }

    public function vedio(Request $request)
    {
        try {
            $app = $this->app(\GuzzleHttp\json_decode(env('WSHH_WECHAT'), true));
            //        集群access_token缓存
            $predis = app('redis')->connection()->client();
            $cache = new RedisCache($predis);
            $app['cache'] = $cache;
            //        集群access_token缓存
            $userData = $app->oauth->user();
        } catch (\Exception $e) {
            return redirect('/game');
        }

        try {
            Redis::select(1);

            if (Redis::command('EXISTS', [$userData['id']]) == 0) {
                $api_token = str_random(60);
                Redis::set($userData['id'], $api_token);
                Redis::select(0);
                Redis::set($api_token, $userData['id'] . ':0');
                Redis::select(2);
                Redis::set($api_token, '0:' . time());
            } else {
                $api_token = Redis::get($userData['id']);
            }
        } catch (\Exception $e) {
            $api_token = '';
        }

        return view('index')->with(['app' => $app, 'api_token' => $api_token]);
    }
}

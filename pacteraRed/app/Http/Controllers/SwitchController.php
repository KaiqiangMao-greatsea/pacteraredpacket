<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Ali_Log;
use App\Http\Controllers\AddSaveUserInfoJob;
use App\Models\WechatUsersInfo;
use App\Jobs\SendPacketJob;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;


class SwitchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function switchRed($num = 0)
    {
        if($num=='mkq'){
            $val = Redis::get('redSwitch');
            var_dump($val);
            return;
        }
        $switchVal = $num;
        Redis::select(0);
        Redis::set('redSwitch', $switchVal);
        $val = Redis::get('redSwitch');
        var_dump($val);
        var_dump('开关状态值：' . boolval($val));
    }

}

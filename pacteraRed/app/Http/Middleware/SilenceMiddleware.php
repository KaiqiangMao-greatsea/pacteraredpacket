<?php

namespace App\Http\Middleware;

use Closure;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;

class SilenceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $access_token = $request->post('access_token');

            if ($access_token) {

                if (Redis::command('EXISTS', [$access_token]) == 1) {
                    return $next($request);
                }
            }

           return response()->json(['coda' => -1]);
        } catch (\Exception $e) {
            return response()->json(['coda' => -1]);
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;

class VisitsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access_token = $request->post('access_token');
        Redis::select(2);

        if ($access_token && Redis::command('EXISTS', [$access_token]) == 1) {
            $data = explode(':', Redis::get($access_token));
            if (isset($data[1])) {
                if (time() - $data[1] <= 7200) {
                    if ($data[0] < 5) {
                        Redis::set($access_token, ++$data[0].':'.$data[1]);
                        return $next($request);
                    } else {
                        return response()->json(['coda' => 0, 'msg' => '访问频率过高']);
                    }
                }
            }
            Redis::set($access_token, '0:'.time());
            return $next($request);
        } else {
            return response()->json(['coda' => 0, 'msg' => 'token no exists']);
        }
    }
}

<?php

namespace App\Jobs;

use App\Models\WechatUsersInfo;
use App\Http\Controllers\Ali_Log;

class SaveUserInfo extends Job
{
    protected $user;
    /**
     * 任务最大尝试次数。
     *
     * @var int
     */
    public $tries = 3;
    /**
     * 任务运行的超时时间。
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * SaveUserInfo constructor.
     * @param $userArr
     * @throws \Exception
     */
    public function __construct($userArr)
    {
        $this->valid($userArr);
    }

    private function valid($userArr)
    {
        $fill = ['openid', 'unionid', 'nickname', 'sex', 'language', 'city', 'province', 'country', 'headimgurl'];
        $tmp = [];
        try {
            foreach ($fill as $v) {
                $tmp[$v] = $userArr[$v];
            }
            if (count($tmp) == 9) {
                $this->user = $tmp;
            }
        } catch (\Exception $e) {
            throw new \Exception('请检查输入的参数 ' . $e->getMessage() . PHP_EOL . json_encode($userArr));
        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WechatUsersInfo $wechatUsersInfo)
    {
//        Ali_Log::info('save user info job action,user info:' . strval(json_encode($this->user)).PHP_EOL);
        //读取json
//        $user = json_decode($this->user);
//        var_dump($this->user);
//        var_dump($wechatUsersInfo);
        $wechatUsersInfo->addWechatUser($this->user);
        print_r('save user info:' . strval(json_encode($this->user).PHP_EOL));
    }
}

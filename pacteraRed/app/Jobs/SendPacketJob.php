<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Redis;
use EasyWeChat\Factory;
use Carbon\Carbon;

class SendPacketJob extends Job
{
    protected $str = '';
    /**
     * 任务最大尝试次数。
     *
     * @var int
     */
//    public $tries = 1;
    /**
     * 每秒钟发送请求数。
     *
     * @var int
     */
    public $count = 30;

    private $keyExpireS = 60 * 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($str)
    {
        //
        $this->str = $str;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('star job :' . $this->str . ' current time ' . time());
        try {

            $time = strtotime(date('Y-m-d H:i', time()));
            $timePerMin = Redis::command('INCR', [$time]);
            if ($timePerMin <= 10) {
                Redis::command('EXPIRE', [$time, $this->keyExpireS]);
            }

            if ($timePerMin >= 1640) {
                print_r('请求频繁' . PHP_EOL);
                $sleeps = time() - $time;
                print_r('sleep' . $sleeps . PHP_EOL);
                sleep($sleeps);
//                dispatch(new self($this->str))->delay(Carbon::now()->addMinutes(1));
//                throw new \Exception('发红包触发1800/min限制');
            }

            $data = explode(':', $this->str);
            $config = [
                'app_id' => 'wx7f4a9ce4e47b8ebe',
                'mch_id' => '1484813662',
                'key' => '79bd840a4e1c4e334a59a51bb6bdf27c',
                'cert_path' => base_path('certificate/apiclient_cert.pem'),
                'key_path' => base_path('certificate/apiclient_key.pem')
            ];
            $payment = Factory::payment($config);

            $redpack = $payment->redpack;
            $redpackData = [
                'mch_billno' => str_random(5) . time(),
                'send_name' => '文思海辉',
                're_openid' => $data[0],
                'total_num' => 1,  //固定为1，可不传
                'total_amount' => number_format($data[1] * 100,0),  //单位为分，不小于100
                'wishing' => '心想事成，旺事如意',
                'client_ip' => '192.168.0.1',  //可不传，不传则由 SDK 取当前客户端 IP
                'act_name' => '数说头条摇一摇活动',
                'remark' => '你参与 文思海辉数说头条摇一摇活动，成功获得文思海辉赠送的红包，点击消息打开，一起抢红包、拼手气吧',
            ];
            $result = $redpack->sendNormal($redpackData);
            Log::info('do send red');

            //test
//            $result['result_code'] = 'FAIL1';
//            $result['err_code'] = '40001';
//            $result['err_code_des'] = 'error test';
            //test
            if ($result['result_code'] == 'FAIL') {
                $str = <<<EOF
openid:{$data[0]}:money:{$data[1]}:time:{$time}:code:{$result['err_code']}:msg:{$result['err_code_des']}"
EOF;
                Log::error($str);
                print_r('error: ' . $str . PHP_EOL);
                Redis::command('LPUSH', ['errorLog', $str]);
                throw new \Exception($str);
            }

        } catch (\Exception $e) {
            $str = 'job error :' . $this->str . ' msg: ' . $e->getMessage() . ' current time: ' . time();
            print_r($str . PHP_EOL);
            Log::info($str);
        }

    }
}

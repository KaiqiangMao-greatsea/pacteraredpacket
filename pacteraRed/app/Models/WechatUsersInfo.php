<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatUsersInfo extends Model
{

    protected $table = 'wechat_users_info';

    protected $fillable = [
        'openid', 'unionid', 'nickname', 'sex', 'language', 'city', 'province', 'country', 'headimgurl', 'status'
    ];

    public function addWechatUser($arr)
    {
        DB::transaction(function () use ($arr) {
            $res = $this->firstOrCreate(['unionid' => $arr['unionid']], $arr);
        }, 3);

    }
}

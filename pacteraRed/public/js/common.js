var designWidth=750;
var fontSize=100;
function resize(){
    var deviceWidth=document.documentElement.clientWidth;
    // console.log("deviceWidth="+deviceWidth);
    var ratio=deviceWidth/designWidth;
    var newfontSize=ratio<0.08?fontSize:fontSize*ratio;
    document.documentElement.style.fontSize=newfontSize+'px';
}
resize();
window.onresize=resize;
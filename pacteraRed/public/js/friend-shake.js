/**
 * Created by gavin on 2018/1/30.
 */
var friendShake = {
    flag:true,
    SHAKE_THRESHOLD:8000,
    last_update:0,
    x:'',
    y:'',
    z:'',
    last_x:0,
    last_y:0,
    last_z:0,
    init:function () {
        // var flag = true;
        // var SHAKE_THRESHOLD = 8000;
        // var last_update = 0;
        // var x, y, z, last_x = 0, last_y = 0, last_z = 0;
        if (window.DeviceMotionEvent) {
            window.addEventListener('devicemotion',friendShake.deviceMotionHandler,false);
        }
    },
    deviceMotionHandler:function(eventData) {
        var acceleration = eventData.accelerationIncludingGravity;
        var curTime = new Date().getTime();
        if ((curTime-friendShake.last_update)> 10) {
            var diffTime = curTime - friendShake.last_update;
            friendShake.last_update = curTime;
            friendShake.x = acceleration.x;
            friendShake.y = acceleration.y;
            friendShake.z = acceleration.z;
            var speed = Math.abs(friendShake.x +friendShake.y + friendShake.z - friendShake.last_x - friendShake.last_y - friendShake.last_z) / diffTime * 10000;
            // var speed = Math.sqrt(( this.x - lastX ) * ( x - lastX ) + ( y - lastY ) * ( y - lastY ) + ( z - lastZ ) * ( z - lastZ ) ) / diffTime * 10000;
            if (speed > friendShake.SHAKE_THRESHOLD && friendShake.flag) {
                alert("你中奖啦！");  // Do something
                friendShake.sendRequest();
                friendShake.flag = false;
            }
            friendShake.last_x = friendShake.x;
            friendShake.last_y = friendShake.y;
            friendShake.last_z = friendShake.z;
        }
    },
    sendRequest:function () {
        $.ajax('/api/send',{
            type:'post',
            success:function(json) {
                alert(json.msg);
                if (json.coda == 1) {

                } else {

                }
            }
        });
    }
}


// function deviceMotionHandler(eventData) {
//     var acceleration =eventData.accelerationIncludingGravity;
//     var curTime = new Date().getTime();
//     if ((curTime-last_update)> 10) {
//         var diffTime = curTime -last_update;
//         last_update = curTime;
//         x = acceleration.x;
//         y = acceleration.y;
//         z = acceleration.z;
//         var speed = Math.abs(x +y + z - last_x - last_y - last_z) / diffTime * 10000;
//         //Math.sqrt(( x - lastX ) * ( x - lastX ) + ( y - lastY ) * ( y - lastY ) + ( z - lastZ ) * ( z - lastZ ) ) / diffTime* //10000;
//         if (speed > SHAKE_THRESHOLD && flag) {
//             // alert("你中奖啦！");  // Do something
//             sendRequest();
//             flag = false;
//         }
//         last_x = x;
//         last_y = y;
//         last_z = z;
//     }
// }

// function sendRequest() {
//     $.ajax('/api/send',{
//         type:'post',
//         success:function(json) {
//             alert(json.msg);
//             if (json.coda == 1) {
//
//             } else {
//
//             }
//         }
//     });
// }
// friendShake.init();

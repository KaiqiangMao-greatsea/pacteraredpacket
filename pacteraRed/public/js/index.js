var vidbox = document.querySelector('.vidbox');
var Movies = document.querySelector('#Movies');
var wshfaudio = document.querySelector('#wshfaudio');
var playvideobtn = document.querySelector('#playvideobtn');
var lodnumber = document.querySelector('#lodnumber');
var lodingpage = document.querySelector('.loading');
var conts = document.querySelector('#conts');
var SkipBtn = document.querySelector('#SkipBtn');
//音频播放状态
var auplay = true;
var friendShake = {
    flag: true,
    SHAKE_THRESHOLD: 12000,
    last_update: 0,
    x: '',
    y: '',
    z: '',
    last_x: 0,
    last_y: 0,
    last_z: 0,
    init: function () {
        // var flag = true;
        // var SHAKE_THRESHOLD = 8000;
        // var last_update = 0;
        // var x, y, z, last_x = 0, last_y = 0, last_z = 0;
        if (window.DeviceMotionEvent) {
            window.addEventListener('devicemotion', friendShake.deviceMotionHandler, false);
        }
    },
    deviceMotionHandler: function (eventData) {
        var acceleration = eventData.accelerationIncludingGravity;
        var curTime = new Date().getTime();
        if ((curTime - friendShake.last_update) > 10) {
            var diffTime = curTime - friendShake.last_update;
            friendShake.last_update = curTime;
            friendShake.x = acceleration.x;
            friendShake.y = acceleration.y;
            friendShake.z = acceleration.z;
            var speed = Math.abs(friendShake.x + friendShake.y + friendShake.z - friendShake.last_x - friendShake.last_y - friendShake.last_z) / diffTime * 10000;
            // var speed = Math.sqrt(( this.x - lastX ) * ( x - lastX ) + ( y - lastY ) * ( y - lastY ) + ( z - lastZ ) * ( z - lastZ ) ) / diffTime * 10000;
            if (speed > friendShake.SHAKE_THRESHOLD && !$('.w-box').hasClass('active') && !$('.nf-box').hasClass('active') && !$('.rp-box').hasClass('active')) {
                // alert("你中奖啦！");  // Do something
                friendShake.sendRequest();
            }
            friendShake.last_x = friendShake.x;
            friendShake.last_y = friendShake.y;
            friendShake.last_z = friendShake.z;
        }
    },
    sendRequest: function () {
        var access_token = $('#access_token').val();
        $('.nf-box').addClass('active');
        if (access_token) {
            if (friendShake.flag) {
                if (sessionStorage.getItem('received') == 1) {
                    $('.rp-box').addClass('active');
                    return false;
                }
                friendShake.flag = false;
                $.ajax('/api/send', {
                    type: 'post',
                    data: {'access_token': access_token},
                    success: function (json) {
                        if (json.coda == 1) {
                            $('.w-box').addClass('active');
                            $('#number1').text(json.msg);
                            sessionStorage.setItem('received', 1);
                        } else if (json.coda == -1) {
                            $('.nf-box').addClass('active');
                        } else if (json.coda == 0) {
                            $('.rp-box').addClass('active');
                        }
                    },
                    complete: function () {
                        friendShake.flag = true;
                    }
                });
            }
        }
    }
}
// if(window.innerWidth/window.innerHeight>750/1660){
//     Movies.classList.add('fullwidth');
// }else{
//     Movies.classList.add('fullheight');
// }
function completeVideo() {
    vidbox.classList.add('active');
    conts.classList.add('active');
    playvideobtn.classList.remove('active');
    // console.log('播放音乐', auplay)
    if (auplay) {
        wshfaudio.play();
    }
    console.log('播放音乐状态', wshfaudio.readyState)
    friendShake.init();
}

function click2Play() {
    playvideobtn.classList.add('active');
    lodnumber.innerText = '100';
}

function MediaStatusAction() {
    var watchVideoStatus4 = setInterval(function () {
        console.log('video status', Movies.readyState)
        if (Movies.readyState == 4) {
            click2Play();
            clearInterval(watchVideoStatus4);
        }
    }, 500)
}

var iosAudioIner = '';
// alert('点OK就行');
// console.log('已开启ios 变态处理方式')
function iosAudioEndedPlay() {
    console.log('进入神奇处理方式')
    if (u.indexOf('Android') > -1 || u.indexOf('Adr') > -1) {
    } else {
        iosAudioIner = setInterval(function () {
            if ($('.content').is(":visible")) {
                completeVideo()
                clearInterval(iosAudioIner)
            }
        }, 700)
    }

}

//百分比加载
$(function () {
    //静止下拉
    $(document).ready(function () {
        function stopScrolling(event) {
            event.preventDefault();
        }

        document.addEventListener('touchmove', stopScrolling, false);
    })
    var u = navigator.userAgent;
    var infonum = 0;
    var perlod = setInterval(percentage, 500);

    function percentage() {
        infonum += Math.round(Math.random() * 10);
        if (infonum >= 98) {
            infonum = 98;
            clearInterval(perlod);
            MediaStatusAction();
        }
        lodnumber.innerText = infonum;
    }

    function onReady() {
        Movies.play();
    }

    $('#Movies').on('onplaying', function () {
        console.log('开始播放视屏');
        lodingpage.classList.remove('active');
    })
    $('#Movies').on('progress', function (e) {
        if (Movies.buffered.length) {
            // console.log(Math.round(e.target.buffered.end(e.target.buffered.length - 1)));
            // lodnumber.innerText=Math.round(e.target.buffered.end(e.target.buffered.length-1));
        }
    })
    if (u.indexOf('Android') > -1 || u.indexOf('Adr') > -1) {
        $('#Movies').on('canplay canplaythrough', function (e) {
            // console.log('安卓 触发 canplay canplaythrough')
            clearInterval(perlod);
            if (infonum <= 98) {
                var perlod2 = setInterval(percentageT, 500);

                function percentageT() {
                    infonum += Math.round(Math.random() * 10);
                    if (infonum >= 100) {
                        click2Play();
                        clearInterval(perlod2);
                        lodnumber.innerText = 100;
                        return;
                    }
                    lodnumber.innerText = infonum;
                }
            } else {
                click2Play();
            }

            // lodingpage.classList.remove('active');
            // Movies.play();
        })
    } else {
        $('#Movies').on('canplaythrough', function (e) {
            // console.log('ios 触发 canplay canplaythrough')
            clearInterval(perlod);
            lodnumber.innerText = '100';
            lodingpage.classList.remove('active');
            Movies.play();
        })
    }

    $('#playvideobtn').click(function () {
        lodingpage.classList.remove('active');
        Movies.play();
    })
    //SkipBtn
    SkipBtn.onclick = function () {
        completeVideo();
        Movies.pause();
    }
    // playback
    $('.pb-btn').on('click', function () {
        wshfaudio.pause();
        reInitVideo();
        // Movies.play();
        vidbox.classList.remove('active');
        iosAudioEndedPlay()
    })
    //closeBtn1
    $('#wincbtn').on('click', function () {
        $('.w-box').removeClass('active');
    })
    //closeBtn2
    $('#nofcbtn').on('click', function () {
        $('.nf-box').removeClass('active');
    })
    //closeBtn3
    $('#repeatbtn').on('click', function () {
        $('.rp-box').removeClass('active');
    })
//  点击分享
    $('.btnp').on('click', function () {
        $('.mask').addClass('active');
    });
//    mask
    $('.mask').on('click', function () {
        $(this).removeClass('active');
    })
//    audioplay
    $('#audioplaybtn').on('click', function () {
        console.log(auplay);
        auplay = !auplay
        $(this).toggleClass('active');
        if (auplay) {
            wshfaudio.play();
        } else {
            wshfaudio.pause();
        }

    })
})

function clearitem() {
    sessionStorage.setItem('received', 0);
}

function setVideoTime(num) {
    Movies.currentTime = num;
    console.log('从新设置播放时间', Movies.currentTime)
}

function reInitVideo() {
    Movies.src = 'http://cdn.18red.pactera.greatseacn.com/h5/18red/cdnfile/18redp.mp4';
    Movies.currentTime = 0;
    Movies.play();
}

$('#Movies').on('ended', function (e) {
    console.log('触发视频播放完毕事件')
    completeVideo();
})
$('#Movies').on('error', function (e) {
    console.log('视频播放遇到错误', e)
    //提示从新加载
    // videoErr();
    errLog(e);
})

// function videoErr() {
//     var loading = weui.loading('视频加载失败请重新尝试');
//     console.log(loading)
//     setTimeout(function () {
//         loading.hide();
//     }, 2500);
// }
// function MS() {
//     setInterval(function () {
//         console.log('视频状态', Movies.readyState)
//     }, 1000)
// }
// MS()




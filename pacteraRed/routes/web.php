<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
//    echo '<pre>';
//    var_dump(app());
//    dd(11,function_exists('config'),config('wechat.app'));
//    return $router->app->version();
//    echo env('RESOURCE_LINK');
//    dd('ok');
});

$router->get('/test','APIController@test');
$router->get('/mkq/test','ExampleController@test');


$router->get('/game','PacteraController@login');

$router->get('/ajax','PacteraTest@login');
//$router->get('/wechat/callbackWSHH','PacteraController@callback');
//$router->get('/wechat/callbackSILENCE','PacteraController@silenceCallBack');
$router->get('/vedio','PacteraController@vedio');

$router->group(['middleware' => 'silence'], function () use ($router) {
    $router->group(['middleware' => 'visits'], function () use ($router) {
        $router->post('/api/send', 'APIController@addRedisList');
    });

    $router->post('/share','APIController@share');
});
$router->get('/vedio2','PacteraController@vedio');
//$router->get('/ELf3JTy3wazvvm5nRsRRtW/assign','PacketNumberController@index');
//$router->get('/ELf3JTy3wazvvkquwerEdv/affirm','APIController@affirm');
//$router->get('/ELf3JTy3wazvvkquwerEdv/add','APIController@add');
//$router->get('/zvvkquwerEdvELf3JTy3wa/switch/{num}','SwitchController@switchRed');
$router->get('/zvvkquwerEdvELf3JTy3wa/redLimit','ExampleController@redLimit');

